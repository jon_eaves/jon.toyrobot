﻿using System;

namespace jon.ToyRobot
{
    public class Result
    {
        private string _message;
        private bool _success;

        public Result(string message, bool success)
        {
            _message = message;
            _success = success;
        }

        public String AsString()
        {
            return _message + ":: " +_success;
        }

        public bool? Success()
        {
            return _success;
        }
    }
}