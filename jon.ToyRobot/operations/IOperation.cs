﻿namespace jon.ToyRobot.operations
{
    public interface IOperation
    {
        string Name();
    }
}