﻿using System;

namespace jon.ToyRobot.operations
{
    public class PlaceOperation : IOperation
    {
        private static string _name = "PLACE";
        private readonly Position _position;
        private readonly Direction _direction;

        public PlaceOperation(Position p, Direction d)
        {
            _position = p;
            _direction = d;
        }

        public string Name()
        {
            return _name;
        }

        public Direction InDirection()
        {
            return _direction;
        }

        public Position AtPosition()
        {
            return _position;
        }
    }
}