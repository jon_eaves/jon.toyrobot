﻿namespace jon.ToyRobot.io
{
    public interface IOutputSink
    {
        void Append(Result rv);
        void Init();
    }
}