﻿using jon.ToyRobot.operations;

namespace jon.ToyRobot.io
{
    public interface IInputSource
    {
        void Init();
        IOperation Next();
    }
}