﻿namespace jon.ToyRobot.table
{
    public class InBoundsValidator : IPositionValidator
    {
        private Table _t;

        public void OnTable(Table t)
        {
            _t = t;
        }

        public bool IsValid(Position position)
        {
            return _t.Includes(position);
        }
    }
}