﻿using System;
using System.Collections.Generic;

namespace jon.ToyRobot.table
{
    public class Table
    {
        private int _width;
        private int _height;
        private List<IPositionValidator> _validators = new List<IPositionValidator>();

        public Table(int width, int height)
        {
            _width = width;
            _height = height;
        }

        public void AddValidator(IPositionValidator validator)
        {
            validator.OnTable(this);
            _validators.Add(validator);
            
        }

        public bool IsValidPosition(Position p)
        {
            bool rv = true;
            foreach (var v in _validators)
            {
                rv &= v.IsValid(p);
            }
            return rv;
        }

        public bool Includes(Position position)
        {
            return ((position.X() >= 0) && (position.X() <= _width) && (position.Y() >= 0) && (position.Y() <= _height));
        }

    }
}