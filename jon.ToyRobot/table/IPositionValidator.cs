﻿namespace jon.ToyRobot.table
{
    public interface IPositionValidator
    {
        void OnTable(Table table);
        bool IsValid(Position position);
    }
}