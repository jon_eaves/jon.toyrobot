﻿using System;
using System.Collections;
using jon.ToyRobot;
using jon.ToyRobot.engines;
using jon.ToyRobot.operations;
using jon.ToyRobot.table;

namespace jon.ToyRobot
{
    public class Robot
    {
        private IEngine _engine;
        private bool _placed;
        private Table _table;
        private Position _position;
        private Direction _direction;

        public Robot(IEngine engine, Table table)
        {
            _engine = engine;
            _table = table;
            _placed = false;
        }

        public Result Operate(IOperation operation)
        {
            if (!_placed)
            {
                if (operation is PlaceOperation)
                {
                    PlaceOperation po = (PlaceOperation) operation;
                    Position atPosition = po.AtPosition();
                    if (_table.IsValidPosition(atPosition))
                    {
                        _direction = po.InDirection();
                        _position = atPosition;
                        _placed = true;
                    }
                }
                return new Result("PLACE CALLED", _placed);
            }


            Position p = _position;
            Direction d = _direction;

            string op = operation.Name();
            switch (operation.Name())
            {
                case "MOVE":
                    p = _engine.Move(_position, _direction);
                    break;
                case "LEFT":
                    d = _engine.Left(_direction);
                    break;
                case "RIGHT":
                    d = _engine.Right(_direction);
                    break;
                case "REPORT":
                    op = "Output: " + ReportOutput();
                    break;
                default:
                    op = "Unknown Operation";
                    break;
            }

            _position = p;
            _direction = d;

            return new Result(op, true);
        }

        private string ReportOutput()
        {
            return _position.ToString() + "," + _direction;
        }
    }
}
