﻿using System.Security.Permissions;

namespace jon.ToyRobot
{
    public class Direction
    {
        public static Direction NORTH = new Direction(0, 1, "NORTH");
        public static Direction SOUTH = new Direction(0, -1, "SOUTH");
        public static Direction WEST = new Direction(-1, 0, "WEST");
        public static Direction EAST = new Direction(1, 0, "EAST");

        private readonly int _xDelta;
        private readonly int _yDelta;
        private readonly string _label;
        private Direction _left;
        private Direction _right;

        static Direction()
        {
            NORTH._left = WEST;
            NORTH._right = EAST;
            SOUTH._left = EAST;
            SOUTH._right = WEST;
            EAST._left = NORTH;
            EAST._right = SOUTH;
            WEST._left = SOUTH;
            WEST._right = NORTH;
        }

        public Direction(int xDelta, int yDelta, string label)
        {
            _xDelta = xDelta;
            _yDelta = yDelta;
            _label = label;
        }

        public Direction Left()
        {
            return _left;
        }

        public Direction Right()
        {
            return _right;
        }

        public int XDelta()
        {
            return _xDelta;
        }

        public int YDelta()
        {
            return _yDelta;
        }

        public override string ToString()
        {
            return _label;
        }
    }
}