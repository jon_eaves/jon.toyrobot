﻿using jon.ToyRobot.operations;

namespace jon.ToyRobot.engines
{
    public class SingleStepEngine : IEngine
    {
        public Position Move(Position position, Direction direction)
        {
            Position current = position;

            int newX = current.X() + direction.XDelta();
            int newY = current.Y() + direction.YDelta();

            return new Position(newX, newY);

        }

        public Direction Left(Direction direction)
        {
            return direction.Left();
        }

        public Direction Right(Direction direction)
        {
            return direction.Right();
        }
    }
}