﻿using jon.ToyRobot.table;

namespace jon.ToyRobot.engines
{
    public class NoFallEngine : IEngine
    {
        private readonly IEngine _motor;
        private readonly Table _table;

        public NoFallEngine(IEngine movementEngine, Table table)
        {
            _motor = movementEngine;
            _table = table;
        }

        public Position Move(Position position, Direction direction)
        {
            Position p = _motor.Move(position, direction);
            if (_table.IsValidPosition(p))
            {
                return p;
            }
            return position;
        }

        public Direction Left(Direction direction)
        {
            return _motor.Left(direction);
        }

        public Direction Right(Direction direction)
        {
            return _motor.Right(direction);
        }
    }
}