﻿using jon.ToyRobot.operations;

namespace jon.ToyRobot.engines
{
    public interface IEngine
    {
        Position Move(Position position, Direction direction);
        Direction Left(Direction direction);
        Direction Right(Direction direction);
    }
}