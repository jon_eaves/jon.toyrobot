﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using jon.ToyRobot.table;
using jon.ToyRobot.UnitTests.table;
using NUnit.Framework;

namespace jon.ToyRobot.UnitTests
{
    [TestFixture]
    public class TableTests
    {
        [Test]
        public void Constructor_WithValidArguments_CanBeCreated()
        {
            // Arrange
            Table table = new Table(1,1);

            // Act
            // -- do nothing

            // Assert
            Assert.NotNull(table);
        }

        [Test]
        public void CallingValidPosition_WithAutoValidator_ReturnsTrue()
        {
            // Arrange
            Table table = new Table(1,1);
            IPositionValidator validator = new EverythingValidValidator();
            table.AddValidator(validator);

            // Act
            Position p = new Position(1,1);

            // Assert
            Assert.True(table.IsValidPosition(p));
        }

        [Test]
        public void CallingValidPosition_WithAutoFailure_ReturnsFalse()
        {
            // Arrange
            Table table = new Table(1,1);
            IPositionValidator validator = new NothingValidValidator();
            table.AddValidator(validator);

            // Act
            Position p = new Position(1,1);

            // Assert
            Assert.False(table.IsValidPosition(p));
            
        }

        [Test]
        public void CallingValidPosition_WithTrueAndFalse_ReturnsFalse()
        {
            // Arrange
            Table table = new Table(1,1);
            table.AddValidator(new NothingValidValidator());
            table.AddValidator(new EverythingValidValidator());

            // Act
            Position p = new Position(1,1);

            // Assert
            Assert.False(table.IsValidPosition(p));
        }
        
    }
}
