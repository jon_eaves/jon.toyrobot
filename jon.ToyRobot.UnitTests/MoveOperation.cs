﻿using jon.ToyRobot.operations;

namespace jon.ToyRobot.UnitTests
{
    internal class MoveOperation : IOperation
    {
        public string Name()
        {
            return "MOVE";
        }
    }
}