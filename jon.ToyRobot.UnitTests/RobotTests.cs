﻿using jon.ToyRobot;
using jon.ToyRobot.engines;
using jon.ToyRobot.table;
using jon.ToyRobot.UnitTests.engines;
using jon.ToyRobot.UnitTests.table;
using NUnit.Framework;

namespace jon.ToyRobot.UnitTests
{
    [TestFixture]
    public class RobotTests
    {
        [Test]
        public void Constructor_WithValidArguments_CanBeCreated()
        {
            // Arrange
            Robot c = new Robot(new NoMovingEngine(), new Table(1,1));

            // Act
            // -- do nothing

            // Assert
            Assert.NotNull(c);
        }

        [Test]
        public void MoveRobot_CurrentlyNotPlaced_DoesNothing()
        {
            Robot robot = new Robot(new NoMovingEngine(), new Table(1, 1));

            Result rv = robot.Operate(new MoveOperation());

            Assert.False(rv.Success());

        }

    }

}
