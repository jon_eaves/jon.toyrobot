﻿using jon.ToyRobot.table;

namespace jon.ToyRobot.UnitTests.table
{
    public class NothingValidValidator : IPositionValidator
    {
        public void OnTable(Table table)
        {
            // do nothing
        }

        public bool IsValid(Position position)
        {
            return false;
        }
    }
}