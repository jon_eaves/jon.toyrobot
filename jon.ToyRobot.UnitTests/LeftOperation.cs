﻿using jon.ToyRobot.operations;

namespace jon.ToyRobot.UnitTests
{
    internal class LeftOperation : IOperation
    {
        public string Name()
        {
            return "LEFT";
        }
    }
}