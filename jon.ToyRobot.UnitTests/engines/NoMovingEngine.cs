﻿using jon.ToyRobot.engines;
using jon.ToyRobot.operations;

namespace jon.ToyRobot.UnitTests.engines
{
    internal class NoMovingEngine : IEngine
    {
        public Position Execute(IOperation operation)
        {
            throw new System.NotImplementedException();
        }

        public Position Move(Position position, Direction direction)
        {
            throw new System.NotImplementedException();
        }

        public Direction Left(Direction direction)
        {
            throw new System.NotImplementedException();
        }

        public Direction Right(Direction direction)
        {
            throw new System.NotImplementedException();
        }
    }
}