﻿using System;
using System.Collections.Generic;
using jon.ToyRobot.io;

namespace jon.ToyRobot.UnitTests.io
{
    public class ResultListOutputSink : IOutputSink
    {
        private List<Result> _results;

        public void Init()
        {
            _results = new List<Result>();
        }

        public void Append(Result rv)
        {
            _results.Add(rv);
        }

        public List<Result> Results()
        {
            return _results;
        }

    }
}