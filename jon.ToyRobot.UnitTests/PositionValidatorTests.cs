﻿using jon.ToyRobot.table;
using jon.ToyRobot.UnitTests.table;
using NUnit.Framework;

namespace jon.ToyRobot.UnitTests
{
    [TestFixture]
    public class PositionValidatorTests
    {
        [Test]
        public void InBoundsValidator_WithValidArguments_CanBeCreated()
        {
            // Arrange
            IPositionValidator validator = new InBoundsValidator();

            // Act
            // -- do nothing

            // Assert
            Assert.NotNull(validator);
        }

        [Test]
        public void CallingValidPosition_WithAutoValidator_ReturnsTrue()
        {
            // Arrange
            IPositionValidator validator = new EverythingValidValidator();
            Table table = new Table(1, 1);
            table.AddValidator(validator);

            // Act
            Position p = new Position(1,1);

            // Assert
            Assert.True(table.IsValidPosition(p));
        }

        [Test]
        public void CallingValidPosition_WithAutoFailure_ReturnsFalse()
        {
            // Arrange
            Table table = new Table(1,1);
            IPositionValidator validator = new NothingValidValidator();
            table.AddValidator(validator);

            // Act
            Position p = new Position(1,1);

            // Assert
            Assert.False(table.IsValidPosition(p));
            
        }

        [Test]
        public void CallingValidPosition_WithTrueAndFalse_ReturnsFalse()
        {
            // Arrange
            Table table = new Table(1,1);
            table.AddValidator(new NothingValidValidator());
            table.AddValidator(new EverythingValidValidator());

            // Act
            Position p = new Position(1,1);

            // Assert
            Assert.False(table.IsValidPosition(p));
        }

        [Test]
        public void CallingValidPosition_WithBoundsValidator_ReturnsTrue()
        {
            Table table = new Table(1,1);
            table.AddValidator(new InBoundsValidator());

            Position p = new Position(1,1);

            Assert.True(table.IsValidPosition(p));
        }

        [Test]
        public void CallingInvalidPosition_WithBoundsValidator_ReturnsFalse()
        {
            Table table = new Table(1,1);
            table.AddValidator(new InBoundsValidator());

            Position p = new Position(5,5);

            Assert.False(table.IsValidPosition(p));
        }

    }
}
