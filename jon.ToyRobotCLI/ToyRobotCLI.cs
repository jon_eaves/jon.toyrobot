﻿using jon.ToyRobot;
using jon.ToyRobot.engines;
using jon.ToyRobot.io;
using jon.ToyRobot.operations;
using jon.ToyRobot.table;
using jon.ToyRobotCLI.io;

namespace jon.ToyRobotCLI
{
    public class ToyRobotCLI
    {
        public static void Main(string[] args)
        {
            ToyRobotCLI runner = new ToyRobotCLI();
            runner.Run();

            System.Console.ReadKey();
        }

        private void Run()
        {
            IInputSource input = new ListOperationInputSource();
            IOutputSink output = new PrintLnOutputSink();

            input.Init();
            output.Init();

            Table t = new Table(5,5);
            IPositionValidator pv = new InBoundsValidator();
            t.AddValidator(pv);
            
            IEngine engine = new NoFallEngine(new SingleStepEngine(), t);

            Robot robot = new Robot(engine, t);

            IOperation o = null;
            while ((o = input.Next()) != null)
            {
                Result rv = robot.Operate(o);
                output.Append(rv);
            }
        }

    }
}
