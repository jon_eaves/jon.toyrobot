﻿using System;
using jon.ToyRobot;
using jon.ToyRobot.io;

namespace jon.ToyRobotCLI.io
{
    public class PrintLnOutputSink : IOutputSink
    {
        public void Init()
        {
            // do nothing
        }

        public void Append(Result rv)
        {
            Console.WriteLine(rv.AsString());
        }

    }
}