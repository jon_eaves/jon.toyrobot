﻿using jon.ToyRobot;
using jon.ToyRobot.io;
using System.Collections.Generic;
using jon.ToyRobot.operations;
using Microsoft.Win32;

namespace jon.ToyRobotCLI.io
{
    internal class ListOperationInputSource : IInputSource
    {
        private readonly List<IOperation> _operations;
        private int _cursor;
        private int _size;

        public ListOperationInputSource()
        {
            _operations = RightWeirdness();
        }

        private List<IOperation> ExampleA()
        {
            List<IOperation> rv = new List<IOperation>();
            rv.Add(new PlaceOperation(new Position(0,0), Direction.NORTH));
            rv.Add(new MoveOperation());
            rv.Add(new ReportOperation());
            return rv;
        }

        private List<IOperation> ExampleC()
        {
            List<IOperation> rv = new List<IOperation>();
            rv.Add(new PlaceOperation(new Position(1, 2), Direction.EAST));
            rv.Add(new MoveOperation());
            rv.Add(new MoveOperation());
            rv.Add(new LeftOperation());
            rv.Add(new MoveOperation());
            rv.Add(new ReportOperation());
            return rv;
        }

        private List<IOperation> RightWeirdness()
        {
            List<IOperation> rv = new List<IOperation>();
            rv.Add(new PlaceOperation(new Position(0,0), Direction.NORTH));
            rv.Add(new MoveOperation());
            rv.Add(new LeftOperation());
            rv.Add(new LeftOperation());
            rv.Add(new LeftOperation());
            rv.Add(new RightOperation());
            rv.Add(new MoveOperation());
            rv.Add(new ReportOperation());
            return rv;
            
        }

        public void Init()
        {
            _cursor = 0;
            _size = _operations.Count;
        }

        public IOperation Next()
        {
            IOperation rv = null;
            if (_cursor < _size)
            {
                rv = _operations[_cursor];
                ++_cursor;
            }
            return rv;
        }
    }
}